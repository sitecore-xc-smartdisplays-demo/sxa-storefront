﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Globalization;
using Sitecore.HabitatHome.Feature.Catalog.Models;
using Sitecore.Links;

namespace Sitecore.HabitatHome.Feature.Catalog.Repositories
{
    public class AlexaResponsePhraseRepository : IAlexaResponsePhraseRepository
    {
        public AlexaResponsePhraseModel GetResponse(string key, string language = "en")
        {
            try
            {
                var responsesRoot = Sitecore.Context.Database.GetItem(new ID("{F743037F-DF46-43CB-B614-C7D78F811286}")); //TODO: Make this configurable
                var responsePhraseItem = responsesRoot.Children.FirstOrDefault(i => i["ResponseKey"].Equals(key, StringComparison.InvariantCultureIgnoreCase));
                
                return new AlexaResponsePhraseModel(responsePhraseItem?.Versions.GetLatestVersion(Data.Managers.LanguageManager.GetLanguage(language)));
            }
            catch (Exception)
            {
                return new AlexaResponsePhraseModel();
            }
        }

        public AlexaResponsePhraseModel GetPersonalisedResponse(string key, string language = "en")
        {
            try
            {
                var responsesRoot = Sitecore.Context.Database.GetItem(new ID("{2062372B-AC37-480E-A3D6-5737021964DC}")); //TODO: Make this configurable
                var responsePhraseItem = responsesRoot.Children.FirstOrDefault(i => i.Name.Equals(key, StringComparison.InvariantCultureIgnoreCase));

                var token = HttpContext.Current.Request.Headers["Authorization"];

                return new AlexaResponsePhraseModel(Sitecore.Web.WebUtil.ExecuteWebPage(LinkManager.GetItemUrl(responsePhraseItem), new NameValueCollection(){{ "Authorization", token ?? string.Empty}}), key, language);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}