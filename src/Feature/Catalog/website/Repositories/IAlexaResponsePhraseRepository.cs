﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.HabitatHome.Feature.Catalog.Models;

namespace Sitecore.HabitatHome.Feature.Catalog.Repositories
{
    public interface IAlexaResponsePhraseRepository
    {
        AlexaResponsePhraseModel GetResponse(string key, string language);
        AlexaResponsePhraseModel GetPersonalisedResponse(string key, string language = "en");
    }
}
