﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Commerce.Services;
using Sitecore.Commerce.XA.Feature.Cart.Repositories;
using Sitecore.Commerce.XA.Feature.Catalog.Controllers;
using Sitecore.Commerce.XA.Feature.Catalog.Models.JsonResults;
using Sitecore.Commerce.XA.Feature.Catalog.Repositories;
using Sitecore.Commerce.XA.Foundation.Common;
using Sitecore.Commerce.XA.Foundation.Common.Context;
using Sitecore.Commerce.XA.Foundation.Common.Models;
using Sitecore.Commerce.XA.Foundation.Common.Models.JsonResults;
using Sitecore.Commerce.XA.Foundation.Common.Search;
using Sitecore.Commerce.XA.Foundation.Connect;
using Sitecore.Commerce.XA.Foundation.Connect.Arguments;
using Sitecore.Commerce.XA.Foundation.Connect.Entities;
using Sitecore.Commerce.XA.Foundation.Connect.Managers;
using Sitecore.DependencyInjection;
using Sitecore.Diagnostics;
using Sitecore.HabitatHome.Feature.Catalog.ActionFilters;

namespace Sitecore.HabitatHome.Feature.Catalog.Controllers
{
    public class AlexaCatalogController : CatalogController
    {
        private IModelProvider _modelProvider;
        private IProductListHeaderRepository _productListHeaderRepository;
        private IProductListRepository _productListRepository;
        private IPromotedProductsRepository _promotedProductsRepository;
        private IProductInformationRepository _productInformationRepository;
        private IProductImagesRepository _productImagesRepository;
        private IProductInventoryRepository _productInventoryRepository;
        private IProductPriceRepository _productPriceRepository;
        private IProductVariantsRepository _productVariantsRepository;
        private IProductBundleRepository _productBundleRepository;
        private IProductListPagerRepository _productListPagerRepository;
        private IProductFacetsRepository _productFacetsRepository;
        private IProductListSortingRepository _productListSortingRepository;
        private IProductListPageInfoRepository _productListPageInfoRepository;
        private IProductListItemsPerPageRepository _productListItemsPerPageRepository;
        private ICatalogItemContainerRepository _catalogItemContainerRepository;
        private IVisitedCategoryPageRepository _visitedCategoryPageRepository;
        private IVisitedProductDetailsPageRepository _visitedProductDetailsPageRepository;
        private ISearchInitiatedRepository _searchInitiatedRepository;

        //context
        private IStorefrontContext _storefrontContext;
        private ISiteContext _siteContext;
        private IContext _sitecoreContext;
        private IVisitorContext _visitorContext;
        
        //managers
        private ISearchManager _searchManager;
        private ICatalogManager _catalogManager;
        private IInventoryManager _inventoryManager;
        private ICartManager _cartManager;


        public AlexaCatalogController(IModelProvider modelProvider,
            IProductListHeaderRepository productListHeaderRepository,
            IProductListRepository productListRepository,
            IPromotedProductsRepository promotedProductsRepository,
            IProductInformationRepository productInformationRepository,
            IProductImagesRepository productImagesRepository,
            IProductInventoryRepository productInventoryRepository,
            IProductPriceRepository productPriceRepository,
            IProductVariantsRepository productVariantsRepository,
            IProductBundleRepository productBundleRepository,
            IProductListPagerRepository productListPagerRepository,
            IProductFacetsRepository productFacetsRepository,
            IProductListSortingRepository productListSortingRepository,
            IProductListPageInfoRepository productListPageInfoRepository,
            IProductListItemsPerPageRepository productListItemsPerPageRepository,
            ICatalogItemContainerRepository catalogItemContainerRepository,
            IVisitedCategoryPageRepository visitedCategoryPageRepository,
            IVisitedProductDetailsPageRepository visitedProductDetailsPageRepository,
            ISearchInitiatedRepository searchInitiatedRepository,
            IStorefrontContext storefrontContext,
            ISiteContext siteContext,
            IContext sitecoreContext,
            IVisitorContext visitorContext,
            ISearchManager searchManager,
            ICatalogManager catalogManager,
            IInventoryManager inventoryManager,
            ICartManager cartManager) : base(modelProvider, productListHeaderRepository,
            productListRepository, promotedProductsRepository, productInformationRepository, productImagesRepository,
            productInventoryRepository, productPriceRepository, productVariantsRepository, productBundleRepository,
            productListPagerRepository, productFacetsRepository, productListSortingRepository,
            productListPageInfoRepository, productListItemsPerPageRepository, catalogItemContainerRepository,
            visitedCategoryPageRepository, visitedProductDetailsPageRepository, searchInitiatedRepository,
            storefrontContext, siteContext, sitecoreContext)
        {

            _modelProvider = modelProvider;
            _productListHeaderRepository = productListHeaderRepository;
            _productListRepository = productListRepository;
            _promotedProductsRepository = promotedProductsRepository;
            _productInformationRepository = productInformationRepository;
            _productImagesRepository = productImagesRepository;
            _productInventoryRepository = productInventoryRepository;
            _productPriceRepository = productPriceRepository;
            _productVariantsRepository = productVariantsRepository;
            _productBundleRepository = productBundleRepository;
            _productListPagerRepository = productListPagerRepository;
            _productFacetsRepository = productFacetsRepository;
            _productListSortingRepository = productListSortingRepository;
            _productListPageInfoRepository = productListPageInfoRepository;
            _productListItemsPerPageRepository = productListItemsPerPageRepository;
            _catalogItemContainerRepository = catalogItemContainerRepository;
            _visitedCategoryPageRepository = visitedCategoryPageRepository;
            _visitedProductDetailsPageRepository = visitedProductDetailsPageRepository;
            _searchInitiatedRepository = searchInitiatedRepository;

            _storefrontContext = storefrontContext;
            _siteContext = siteContext;
            _sitecoreContext = sitecoreContext;
            _visitorContext = visitorContext;

            _searchManager = searchManager;
            _catalogManager = catalogManager;
            _inventoryManager = inventoryManager;
            _cartManager = cartManager;
        }


        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult WakeUp()
        {
            return Json("I'm up!'", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult GetProducts(string searchKeyword)
        {
            var searchResults = _searchManager.SearchCatalogItemsByKeyword(searchKeyword, _storefrontContext.CurrentStorefront.Catalog, new CommerceSearchOptions());

            var productEntityList = this.AdjustProductPriceAndStockStatus(searchResults).ToList();
            var productListJsonResult = _modelProvider.GetModel<ProductListJsonResult>();
            productListJsonResult.Initialize(null, productEntityList, false, searchKeyword);

            return Json(productListJsonResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult GetProductStockInfo(string productId)
        {
            var productStockInformation = this.ProductInventoryRepository.GetProductStockInformation(productId);

            return Json(productStockInformation, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<ProductEntity> AdjustProductPriceAndStockStatus(SearchResults searchResult)
        {
            Assert.ArgumentNotNull((object)searchResult, nameof(searchResult));
            var currentStorefront = _storefrontContext.CurrentStorefront;
            var productEntityList = new List<ProductEntity>();

            if (searchResult.SearchResultItems == null || searchResult.SearchResultItems.Count <= 0)
                return productEntityList;

            foreach (var searchResultItem in searchResult.SearchResultItems)
            {
                var model = _modelProvider.GetModel<ProductEntity>();
                model.Initialize(currentStorefront, searchResultItem);
                productEntityList.Add(model);
            }
            _catalogManager.GetProductBulkPrices(currentStorefront, _visitorContext, productEntityList);
            _inventoryManager.GetProductsStockStatus(currentStorefront, productEntityList, currentStorefront.UseIndexFileForProductStatusInLists);
            foreach (var productEntity1 in productEntityList)
            {
                var productEntity = productEntity1;
                var productItem = searchResult.SearchResultItems.FirstOrDefault(item =>
                {
                    if (item.Name == productEntity.ProductId)
                        return item.Language == Sitecore.Context.Language;
                    return false;
                });
                if (productItem != null)
                    productEntity.CustomerAverageRating = _catalogManager.GetProductRating(productItem);
            }

            return productEntityList;
        }
    }
}