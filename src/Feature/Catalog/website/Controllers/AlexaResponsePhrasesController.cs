﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Sitecore.Commerce.Services;
using Sitecore.Commerce.XA.Feature.Cart.Repositories;
using Sitecore.Commerce.XA.Feature.Catalog.Controllers;
using Sitecore.Commerce.XA.Feature.Catalog.Models.JsonResults;
using Sitecore.Commerce.XA.Feature.Catalog.Repositories;
using Sitecore.Commerce.XA.Foundation.Common;
using Sitecore.Commerce.XA.Foundation.Common.Context;
using Sitecore.Commerce.XA.Foundation.Common.Models;
using Sitecore.Commerce.XA.Foundation.Common.Models.JsonResults;
using Sitecore.Commerce.XA.Foundation.Common.Search;
using Sitecore.Commerce.XA.Foundation.Connect;
using Sitecore.Commerce.XA.Foundation.Connect.Arguments;
using Sitecore.Commerce.XA.Foundation.Connect.Entities;
using Sitecore.Commerce.XA.Foundation.Connect.Managers;
using Sitecore.DependencyInjection;
using Sitecore.Diagnostics;
using Sitecore.HabitatHome.Feature.Catalog.ActionFilters;
using Sitecore.HabitatHome.Feature.Catalog.Models;
using Sitecore.HabitatHome.Feature.Catalog.Repositories;

namespace Sitecore.HabitatHome.Feature.Catalog.Controllers
{
    public class AlexaResponsePhrasesController : Controller
    {
        private IAlexaResponsePhraseRepository _alexaResponsePhraseRepository;

        //context
        private IStorefrontContext _storefrontContext;
        //private ISiteContext _siteContext;
        private IContext _sitecoreContext;
        private IVisitorContext _visitorContext;

        private IOrderManager _orderManager;

        public AlexaResponsePhrasesController(IAlexaResponsePhraseRepository alexaResponsePhraseRepository, IVisitorContext visitorContext, IStorefrontContext storefrontContext, IContext sitecoreContext, IOrderManager orderManager)
        {
            _alexaResponsePhraseRepository = alexaResponsePhraseRepository;

            _storefrontContext = storefrontContext;
            _sitecoreContext = sitecoreContext;
            _visitorContext = visitorContext;

            _orderManager = orderManager;
        }
        
        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult GetProducts(string language)
        {
            return Json(_alexaResponsePhraseRepository.GetResponse("GetProducts", language), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult GetShoppingCart(string language)
        {
            return Json(_alexaResponsePhraseRepository.GetResponse("GetShoppingCart", language), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult AddCartLine(string language)
        {
            return Json(_alexaResponsePhraseRepository.GetResponse("AddCartLine", language), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult CompleteCheckout(string language)
        {
            return Json(_alexaResponsePhraseRepository.GetResponse("CompleteCheckout", language), JsonRequestBehavior.AllowGet);
        }
    }
}