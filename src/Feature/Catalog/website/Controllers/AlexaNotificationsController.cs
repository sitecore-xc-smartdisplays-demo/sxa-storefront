﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Extensions.Logging;
using Sitecore.Commerce.XA.Foundation.Common.Context;
using Sitecore.Commerce.XA.Foundation.Common.Models.JsonResults;
using Sitecore.Commerce.XA.Foundation.Connect;
using Sitecore.Data;
using Sitecore.HabitatHome.Feature.Catalog.ActionFilters;
using Sitecore.HabitatHome.Feature.Catalog.Helpers;
using Sitecore.HabitatHome.Feature.Catalog.Personalization;
using Sitecore.HabitatHome.Feature.Catalog.Services;
using Sitecore.XConnect.Collection.Model;
using Sitecore.Xdb.MarketingAutomation.Core.Activity;

namespace Sitecore.HabitatHome.Feature.Catalog.Controllers
{
    public class AlexaNotificationsController : Controller
    {
        private IStorefrontContext _storefrontContext;
        private IContext _sitecoreContext;

        public AlexaNotificationsController(IContext sitecoreContext, IStorefrontContext storefrontContext)
        {
            _sitecoreContext = sitecoreContext;
            _storefrontContext = storefrontContext;

        }

        [HttpPost]
        [AlexaUserAuthorization]
        public JsonResult SetAmazonUserAccount(string userAccountId)
        {
            var model = new BaseJsonResult(_sitecoreContext, _storefrontContext);

            try
            {
                KnownContactsHelper.SetCommerceContactPersonalInformation(userAccountId);
                model.Success = true;
            }
            catch (Exception ex)
            {
                model.SetErrors(new List<string>(){ex.Message});
            }

            return base.Json(model);
        }

        [HttpPost]
        [AlexaUserAuthorization]
        public JsonResult SendAlexaNotification()
        {
            var model = new BaseJsonResult(_sitecoreContext, _storefrontContext);

            try
            {
                var contact = KnownContactsHelper.GetCurrentXConnectContact();

                var personalInformation = contact?.Personal();
                var contactEmail = contact?.Emails()?.PreferredEmail?.SmtpAddress;

                if (string.IsNullOrEmpty(contactEmail))
                {
                    model.Success = false;
                }

                var alexaNotificationService = new AlexaNotificationService();
                alexaNotificationService.SendAbandonedCartNotificationEx(personalInformation?.Nickname);

                model.Success = true;
            }
            catch (Exception ex)
            {
                model.SetErrors(new List<string>() { ex.Message });
            }

            return base.Json(model);
        }
    }
}