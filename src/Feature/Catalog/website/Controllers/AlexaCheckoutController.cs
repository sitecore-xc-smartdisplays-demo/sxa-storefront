﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Sitecore.Commerce.Engine.Connect.Entities;
using Sitecore.Commerce.Entities.Carts;
using Sitecore.Commerce.Services;
using Sitecore.Commerce.XA.Feature.Cart.Controllers;
using Sitecore.Commerce.XA.Feature.Cart.Models.InputModels;
using Sitecore.Commerce.XA.Feature.Cart.Models.JsonResults;
using Sitecore.Commerce.XA.Feature.Cart.Repositories;
using Sitecore.Commerce.XA.Foundation.Common;
using Sitecore.Commerce.XA.Foundation.Common.Context;
using Sitecore.Commerce.XA.Foundation.Common.Models;
using Sitecore.Commerce.XA.Foundation.Common.Models.JsonResults;
using Sitecore.Commerce.XA.Foundation.Connect;
using Sitecore.Commerce.XA.Foundation.Connect.Managers;
using Sitecore.HabitatHome.Feature.Catalog.ActionFilters;

namespace Sitecore.HabitatHome.Feature.Catalog.Controllers
{
    public class AlexaCheckoutController : CheckoutController
    {
        private IModelProvider _modelProvider;
        private IAddToCartRepository _addToCartRepository;
        private IMinicartRepository _minicartRepository;
        private IPromotionCodesRepository _promotionCodesRepository;
        private IShoppingCartLinesRepository _shoppingCartLinesRepository;
        private IShoppingCartTotalRepository _shoppingCartTotalRepository;

        //context
        private IStorefrontContext _storefrontContext;
        private ISiteContext _siteContext;
        private IContext _sitecoreContext;
        private IVisitorContext _visitorContext;

        //managers
        private ICartManager _cartManager;
        private IOrderManager _orderManager;

        public AlexaCheckoutController(IModelProvider modelProvider, 
            IStartCheckoutRepository startCheckoutRepository, 
            IStepIndicatorRepository stepIndicatorRepository,
            IDeliveryRepository deliveryRepository, 
            IBillingRepository billingRepository,
            IReviewRepository reviewRepository, 
            IOrderConfirmationRepository orderConfirmationRepository,
            IStorefrontContext storefrontContext,
            ISiteContext siteContext,
            IContext sitecoreContext,
            IVisitorContext visitorContext,
            ICartManager cartManager,
            IOrderManager orderManager) : base(storefrontContext, startCheckoutRepository, stepIndicatorRepository,
            deliveryRepository, billingRepository, reviewRepository, orderConfirmationRepository, sitecoreContext)
        {
            _modelProvider = modelProvider;

            _storefrontContext = storefrontContext;
            _siteContext = siteContext;
            _sitecoreContext = sitecoreContext;
            _visitorContext = visitorContext;

            _cartManager = cartManager;
            _orderManager = orderManager;
        }

        [HttpPost]
        [AlexaUserAuthorization]
        public JsonResult CompleteCheckout()
        {
            var retryCount = 0;
            var maxRetryCount = 1;
            var model = new BaseJsonResult(_sitecoreContext, _storefrontContext);

            try
            {
                SubmitOrderJsonResult submitCurrentVisitorOrderResult = null;

                while (retryCount < maxRetryCount)
                {
                    var currentStorefront = _storefrontContext.CurrentStorefront;
                    var currentCart = _cartManager.GetCurrentCart(_visitorContext, _storefrontContext, false);
                    if (!currentCart.ServiceProviderResult.Success || currentCart.Result == null)
                    {
                        var systemMessage = _storefrontContext.GetSystemMessage("Cart Not Found Error", true);
                        currentCart.ServiceProviderResult.SystemMessages.Add(new SystemMessage
                        {
                            Message = systemMessage
                        });
                        model.SetErrors(currentCart.ServiceProviderResult);
                        return base.Json(model);
                    }

                    var cart = currentCart.Result;


                    //Set Standard shipping method
                    //TODO: Hardcoded for testing purposes. Make this dynamic based on user's account data
                    var shippingMethod = new ShippingMethodInputModel()
                    {
                        ShippingMethodID = "cf0af82a-e1b8-45c2-91db-7b9847af287c",
                        ShippingMethodName = "Standard",
                        ShippingPreferenceType = "1",
                        PartyID = "83f57699c39645759de1f844a297f3c7"
                    };
                    var partyInputModel = new PartyInputModel()
                    {
                        Name = "Habitat Grocer",
                        Address1 = "101 California Street, Floor 16",
                        Country = "US",
                        City = "San Francisco",
                        State = "CA",
                        ZipPostalCode = "94111",
                        ExternalId = "83f57699c39645759de1f844a297f3c7",
                        PartyId = "83f57699c39645759de1f844a297f3c7"
                    };

                    var setShippingMethodsInputModel = new SetShippingMethodsInputModel()
                    {
                        DeliveryItemPath = "/sitecore/content/Sitecore/Storefront/Home/checkout/delivery",
                        OrderShippingPreferenceType = "1",
                        ShippingMethods = new List<ShippingMethodInputModel>() { shippingMethod },
                        ShippingAddresses = new List<PartyInputModel>() { partyInputModel }
                    };
                    _cartManager.RemoveAllShippingMethods(currentStorefront, _visitorContext, cart);
                    var setShippingMethodsResult = this.DeliveryRepository.SetShippingMethods(this.VisitorContext, setShippingMethodsInputModel);
                    if (!setShippingMethodsResult.Success && setShippingMethodsResult.HasErrors)
                    {
                        var systemMessage = _storefrontContext.GetSystemMessage("Error setting shipping method", true);
                        currentCart.ServiceProviderResult.SystemMessages.Add(new SystemMessage
                        {
                            Message = systemMessage
                        });
                        model.SetErrors(currentCart.ServiceProviderResult);
                        return base.Json(model);
                    }

                    //Refresh cart
                    currentCart = _cartManager.GetCurrentCart(_visitorContext, _storefrontContext, false);
                    if (!currentCart.ServiceProviderResult.Success || currentCart.Result == null)
                    {
                        var systemMessage = _storefrontContext.GetSystemMessage("Cart Not Found Error", true);
                        currentCart.ServiceProviderResult.SystemMessages.Add(new SystemMessage
                        {
                            Message = systemMessage
                        });
                        model.SetErrors(currentCart.ServiceProviderResult);
                        return base.Json(model);
                    }

                    cart = currentCart.Result;

                    //Set payment method
                    //TODO: Hardcoded for testing purposes. Make this dynamic based on user's account data
                    var federatedPaymentInputModel = new FederatedPaymentInputModel()
                    {
                        CardToken = "fake-valid-nonce",
                        Amount = cart.Total.Amount + cart.Adjustments.Sum(i => i.Amount),
                        CardPaymentAcceptCardPrefix = "paypal",
                        PaymentMethodID = "69bc874b7d3e4170bfd2a47bdf91b553"
                    };
                    var paymentInputModel = new PaymentInputModel()
                    {
                        BillingItemPath = "/sitecore/content/Sitecore/Storefront/Home/checkout/billing",
                        UserEmail = "user@email.com",
                        FederatedPayment = federatedPaymentInputModel,
                        BillingAddress = partyInputModel
                    };

                    _cartManager.RemoveAllPaymentMethods(currentStorefront, _visitorContext, cart);
                    var setPaymentMethodsResult = this.BillingRepository.SetPaymentMethods(this.VisitorContext, paymentInputModel);
                    if (!setPaymentMethodsResult.Success && setPaymentMethodsResult.HasErrors)
                    {
                        var systemMessage = _storefrontContext.GetSystemMessage("Error setting payment method", true);
                        currentCart.ServiceProviderResult.SystemMessages.Add(new SystemMessage
                        {
                            Message = systemMessage
                        });
                        model.SetErrors(currentCart.ServiceProviderResult);
                        return base.Json(model);
                    }


                    //Submit order
                    var submitVisitorOrderInputModel = new SubmitVisitorOrderInputModel()
                    {
                        ConfirmItemPath = "//sitecore/content/Sitecore/Storefront/Home/checkout/review"
                    };

                    retryCount++;
                    submitCurrentVisitorOrderResult = this.ReviewRepository.SubmitCurrentVisitorOrder(this.VisitorContext, submitVisitorOrderInputModel);
                    if (!submitCurrentVisitorOrderResult.HasErrors) break;
                }
                
                return base.Json(submitCurrentVisitorOrderResult);
            }
            catch (Exception)
            {
                model.Success = true;
                return base.Json(model);
            }
        }

        private void SubmitOrder()
        {

        }

        
    }
}