﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Sitecore.Commerce.Entities.Carts;
using Sitecore.Commerce.Services;
using Sitecore.Commerce.XA.Feature.Cart.Controllers;
using Sitecore.Commerce.XA.Feature.Cart.Repositories;
using Sitecore.Commerce.XA.Foundation.Common;
using Sitecore.Commerce.XA.Foundation.Common.Context;
using Sitecore.Commerce.XA.Foundation.Common.Models;
using Sitecore.Commerce.XA.Foundation.Common.Models.JsonResults;
using Sitecore.Commerce.XA.Foundation.Connect;
using Sitecore.Commerce.XA.Foundation.Connect.Arguments;
using Sitecore.Commerce.XA.Foundation.Connect.Managers;
using Sitecore.HabitatHome.Feature.Catalog.ActionFilters;
using Sitecore.HabitatHome.Feature.Catalog.Factories;
using Sitecore.HabitatHome.Feature.Catalog.Helpers;
using Sitecore.HabitatHome.Feature.Catalog.Personalization;

namespace Sitecore.HabitatHome.Feature.Catalog.Controllers
{
    public class AlexaCartController : CartController
    {
        private IModelProvider _modelProvider;
        private IAddToCartRepository _addToCartRepository;
        private IMinicartRepository _minicartRepository;
        private IPromotionCodesRepository _promotionCodesRepository;
        private IShoppingCartLinesRepository _shoppingCartLinesRepository;
        private IShoppingCartTotalRepository _shoppingCartTotalRepository;

        //context
        private IStorefrontContext _storefrontContext;
        private ISiteContext _siteContext;
        private IContext _sitecoreContext;
        private IVisitorContext _visitorContext;

        //managers
        private ICartManager _cartManager;


        public AlexaCartController(IModelProvider modelProvider,
            IAddToCartRepository addToCartRepository, 
            IMinicartRepository minicartRepository,
            IPromotionCodesRepository promotionCodesRepository,
            IShoppingCartLinesRepository shoppingCartLinesRepository,
            IShoppingCartTotalRepository shoppingCartTotalRepository,
            IStorefrontContext storefrontContext,
            ISiteContext siteContext,
            IContext sitecoreContext,
            IVisitorContext visitorContext,
            ICartManager cartManager) : base(
            storefrontContext, modelProvider, addToCartRepository, minicartRepository, promotionCodesRepository,
            shoppingCartLinesRepository, shoppingCartTotalRepository, sitecoreContext)
        {
            _modelProvider = modelProvider;
            _addToCartRepository = addToCartRepository;
            _minicartRepository = minicartRepository;
            _promotionCodesRepository = promotionCodesRepository;
            _shoppingCartLinesRepository = shoppingCartLinesRepository;
            _shoppingCartTotalRepository = shoppingCartTotalRepository;

            _storefrontContext = storefrontContext;
            _siteContext = siteContext;
            _sitecoreContext = sitecoreContext;
            _visitorContext = visitorContext;

            _cartManager = cartManager;
        }

        [HttpPost]
        [AlexaUserAuthorization]
        public JsonResult AddCartLine(string productId, string variantId, string quantity = "1.0")
        {
            var model = new BaseJsonResult(_sitecoreContext, _storefrontContext);
            var currentStorefront = _storefrontContext.CurrentStorefront;
            var currentCart = _cartManager.GetCurrentCart(_visitorContext, _storefrontContext, false);
            if (!currentCart.ServiceProviderResult.Success || currentCart.Result == null)
            {
                var systemMessage = _storefrontContext.GetSystemMessage("Cart Not Found Error", true);
                currentCart.ServiceProviderResult.SystemMessages.Add(new SystemMessage
                {
                    Message = systemMessage
                });
                model.SetErrors(currentCart.ServiceProviderResult);
                return base.Json(model);
            }

            //Set email on cart
            var username = Sitecore.Context.GetUserName();
            try
            {
                var email1 = Sitecore.Context.User.Profile.GetCustomProperty("Has Password Reset");
                var email2 = Membership.GetUser()?.Email;

                var updateCartResponse = _cartManager.UpdateCart(StorefrontContext.CurrentStorefront, currentCart.Result, new CartBase { Email = email2 });
                if (!updateCartResponse.ServiceProviderResult.Success)
                {
                    model.SetErrors(updateCartResponse.ServiceProviderResult);
                    return model;
                }
            }
            catch (Exception ex)
            {

                Sitecore.Diagnostics.Log.Error($"Unable to set email for user {username}", this);
            }


            //Add line
            var list = new List<CartLineArgument>
            {
                new CartLineArgument
                {
                    CatalogName = _storefrontContext.CurrentStorefront.Catalog,
                    ProductId = productId,
                    VariantId = variantId,
                    Quantity = decimal.Parse(quantity)
                }
            };

            try
            {
                var managerResponse = _cartManager.AddLineItemsToCart(currentStorefront, _visitorContext, currentCart.Result, list);
                if (!managerResponse.ServiceProviderResult.Success)
                {
                    model.SetErrors(managerResponse.ServiceProviderResult);
                    return base.Json(model);
                }
            }
            catch (Exception)
            {

                
            }
            model.Success = true;
            return base.Json(model);
        }

        [HttpPost]
        [AlexaUserAuthorization]
        public JsonResult RemoveAllCartLines()
        {
            var model = new BaseJsonResult(_sitecoreContext, _storefrontContext);
            var currentStorefront = _storefrontContext.CurrentStorefront;
            var currentCart = _cartManager.GetCurrentCart(_visitorContext, _storefrontContext, false);
            if (!currentCart.ServiceProviderResult.Success || currentCart.Result == null)
            {
                var systemMessage = _storefrontContext.GetSystemMessage("Cart Not Found Error", true);
                currentCart.ServiceProviderResult.SystemMessages.Add(new SystemMessage
                {
                    Message = systemMessage
                });
                model.SetErrors(currentCart.ServiceProviderResult);
                return base.Json(model);
            }
            
            var cart = currentCart.Result;
            try
            {
                var managerResponse = _cartManager.RemoveLineItemsFromCart(currentStorefront, _visitorContext, cart, cart.Lines.Select(i => i.ExternalCartLineId));
                if (!managerResponse.ServiceProviderResult.Success)
                {
                    model.SetErrors(managerResponse.ServiceProviderResult);
                    return base.Json(model);
                }
            }
            catch (Exception)
            {

            }
            model.Success = true;
            return base.Json(model);
        }

        [HttpGet]
        [AlexaUserAuthorization]
        public JsonResult GetShoppingCart()
        {   
            var model = GetShoppingCartLines();
            return base.Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}