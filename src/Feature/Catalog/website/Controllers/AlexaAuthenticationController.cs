﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.HabitatHome.Feature.Catalog.Helpers;

namespace Sitecore.HabitatHome.Feature.Catalog.Controllers
{
    public class AlexaAuthenticationController : Controller
    {
        [HttpGet]
        public JsonResult GetToken(string username, string password)
        {
            var token = UserHelper.LoginUsingIdentityServer(username, password);

            return base.Json(token, JsonRequestBehavior.AllowGet);
            ;
        }
    }

}