﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using Sitecore.Data.Items;

namespace Sitecore.HabitatHome.Feature.Catalog.Models
{
    public class AlexaResponsePhraseModel
    {
        public AlexaResponsePhraseModel()
        {

        }

        public AlexaResponsePhraseModel(Item item)
        {
            ResponseKey = item["ResponseKey"];
            Language = item.Language.Name;
            SpeakOutput = item["SpeakOutput"];
            RepromptText = item["RepromptText"];
        }

        public AlexaResponsePhraseModel(string html, string responseKey, string language)
        {
            ResponseKey = responseKey;
            Language = language;

            var doc = new HtmlDocument();
            doc.LoadHtml(html);

            var responseContent = doc.DocumentNode.SelectSingleNode("//div[@class='component-content']").InnerHtml.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            if(responseContent.Length > 0) SpeakOutput = FormatResponse(responseContent[0]);
            if(responseContent.Length > 1) RepromptText = FormatResponse(responseContent[1]);
        }

        private string FormatResponse(string content)
        {
            return content.Trim().Replace(System.Environment.NewLine, string.Empty);
        }

        public string ResponseKey { get; set; }
        public string Language { get; set; }
        public string SpeakOutput { get; set; }
        public string RepromptText { get; set; }
    }
}