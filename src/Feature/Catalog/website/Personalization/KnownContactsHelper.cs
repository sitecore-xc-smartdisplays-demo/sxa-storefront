﻿using System;
using Sitecore.Analytics.Model;
using Sitecore.Analytics.Model.Entities;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Collection.Model;
using System.Linq;
using System.Threading.Tasks;
using Sitecore.Diagnostics;

namespace Sitecore.HabitatHome.Feature.Catalog.Personalization
{
    public static class KnownContactsHelper
    {
        public static void SetCommerceContactPersonalInformation(string nickname)
        {
            var manager = Sitecore.Configuration.Factory.CreateObject("tracking/contactManager", true) as Sitecore.Analytics.Tracking.ContactManager;

            if (!Sitecore.Analytics.Tracker.Current.Contact.IsNew)
            {
                var commerceContactIdentifier = Sitecore.Analytics.Tracker.Current.Contact.Identifiers.FirstOrDefault(i => i.Source.Equals("CommerceUser"));

                // Get contact from xConnect, update and save the facet
                using (XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
                {
                    try
                    {
                        var contact = client.Get<Contact>(new IdentifiedContactReference(commerceContactIdentifier.Source, commerceContactIdentifier.Identifier), new Sitecore.XConnect.ContactExpandOptions(PersonalInformation.DefaultFacetKey));

                        if (contact != null)
                        {
                            if (contact.Personal() != null)
                            {
                                contact.Personal().Nickname = nickname;

                                client.SetFacet<PersonalInformation>(contact, PersonalInformation.DefaultFacetKey, contact.Personal());
                            }
                            else
                            {
                                client.SetFacet<PersonalInformation>(contact, PersonalInformation.DefaultFacetKey, new PersonalInformation()
                                {
                                    Nickname = nickname
                                });
                            }

                            client.Submit();

                            // Remove contact data from shared session state - contact will be re-loaded
                            // during subsequent request with updated facets
                            manager.RemoveFromSession(Sitecore.Analytics.Tracker.Current.Contact.ContactId);
                            Sitecore.Analytics.Tracker.Current.Session.Contact = manager.LoadContact(Sitecore.Analytics.Tracker.Current.Contact.ContactId);
                        }
                    }
                    catch (XdbExecutionException ex)
                    {
                        Log.Error($"Error updating facet for contact. Error: {ex.Message}", ex, typeof(KnownContactsHelper));
                        throw;
                    }
                }
            }
        }

        public static Contact GetCurrentXConnectContact()
        {
            try
            {
                if (!Sitecore.Analytics.Tracker.Current.Contact.IsNew)
                {
                    var commerceIdentifier = Sitecore.Analytics.Tracker.Current.Contact.Identifiers.FirstOrDefault(i => i.Source.Equals("CommerceUser"));

                    // Get contact from xConnect, update and save the facet
                    using (XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
                    {
                        var contact = client.Get<Contact>(new IdentifiedContactReference(commerceIdentifier.Source, commerceIdentifier.Identifier), new Sitecore.XConnect.ContactExpandOptions(new string[] { PersonalInformation.DefaultFacetKey, EmailAddressList.DefaultFacetKey }));

                        return contact;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Log.Error($"Error geting current contact. Error: {ex.Message}", ex, typeof(KnownContactsHelper));
                return null;
            }
        }

        private static void GetContactbyId(string contactId)
        {
            using (Sitecore.XConnect.Client.XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            {
                try
                {
                    var reference = new Sitecore.XConnect.ContactReference(Guid.Parse(contactId));
                    
                    var contact = client.Get<Contact>(reference, new Sitecore.XConnect.ContactExpandOptions() { });

                    var identifier = contact.Identifiers.FirstOrDefault();
                }
                catch (XdbExecutionException ex)
                {
                    // Manage exceptions
                }
            }
        }


        private static Contact GetContactByCommerceIdentifier(string username)
        {
            using (XConnectClient client = Sitecore.XConnect.Client.Configuration.SitecoreXConnectClientConfiguration.GetClient())
            {
                try
                {
                    var reference = new IdentifiedContactReference("CommerceUser", username);

                    return client.Get<Contact>(reference, new ContactExpandOptions() { });
                }
                catch (XdbExecutionException ex)
                {
                    Sitecore.Diagnostics.Log.Error($"Error getting contact by commerce identifier for {username}", ex, typeof(KnownContactsHelper));
                    return null;
                }
            }
        }


        public static void SetCurrentContact()
        {
            var username = Sitecore.Context.User.Name;
            try
            {
                var contact = GetContactByCommerceIdentifier(username);
                if (contact != null)
                {
                    //Sitecore.Analytics.Tracker.Current.Session.Contact = contact;
                }

                var manager = Sitecore.Configuration.Factory.CreateObject("tracking/contactManager", true) as Sitecore.Analytics.Tracking.ContactManager;

                if (manager != null)
                {
                    var knownContact = manager.LoadContact("CommerceUser", username);
                    Sitecore.Analytics.Tracker.Current.Session.Contact = knownContact;
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error($"Error settting current contact to  {username}.", ex, typeof(KnownContactsHelper));
            }
        }
    }
}