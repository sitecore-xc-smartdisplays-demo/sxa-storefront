﻿1. Add custom activity to:
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\jobs\continuous\AutomationEngine\App_Data\Config\sitecore\MarketingAutomation\sc.Commerce.MarketingAutomation.ActivityTypes.xml

2. Register custom AlexaNotificationService in:
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\Config\Sitecore\MarketingAutomation\sc.MarketingAutomation.ActivityServices.xml
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\jobs\continuous\AutomationEngine\App_Data\Config\sitecore\MarketingAutomation\sc.MarketingAutomation.ActivityServices.xml

3. Register any custom facets in:
sc.MarketingAutomation.ContactLoader.xml file

4. Update UI



*References:
https://dogabenli.blogspot.com/2019/02/sitecore-9-creating-custom-marketing.html
https://dogabenli.blogspot.com/2019/02/sitecore-9-creating-custom-marketing_16.html



------------


NOTE- To only change the backend and re-use commerce send email activity OOTB:

1. Update SendEmail actvity to reference your extended implementation type
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\jobs\continuous\AutomationEngine\App_Data\Config\sitecore\MarketingAutomation\sc.Commerce.MarketingAutomation.ActivityTypes.xml

Updated binary to:
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\jobs\continuous\AutomationEngine

2. Register custom AlexaNotificationService in:
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\Config\Sitecore\MarketingAutomation\sc.MarketingAutomation.ActivityServices.xml
C:\inetpub\wwwroot\xconnect.xc-smartdisplays-demo.com\App_Data\jobs\continuous\AutomationEngine\App_Data\Config\sitecore\MarketingAutomation\sc.MarketingAutomation.ActivityServices.xml

3. Register any custom facets in:
sc.MarketingAutomation.ContactLoader.xml file
