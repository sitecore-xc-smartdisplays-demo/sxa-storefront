﻿using Microsoft.Extensions.Logging;
using Sitecore.Framework.Conditions;
using Sitecore.XConnect.Schema;
using Sitecore.Xdb.MarketingAutomation.Core.Activity;
using Sitecore.Xdb.MarketingAutomation.Core.Processing.Plan;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sitecore.Commerce.MarketingAutomation.Activities;
using Sitecore.EmailCampaign.Model.Messaging;
using Sitecore.EmailCampaign.Model.Messaging.Buses;
using Sitecore.Framework.Messaging;
using Sitecore.HabitatHome.Feature.Catalog.Services;
using Sitecore.XConnect.Collection.Model;


namespace Sitecore.HabitatHome.Feature.Catalog.MarketingAutomation.Activities
{
    public class SendAlexaNotification : IActivity
    {
        private readonly IMessageBus<AutomatedMessagesBus> _automatedMessageBus;

        protected ILogger<IActivity> Logger { get; }

        public IActivityServices Services { get; set; }

        public Guid MessageId { get; set; }

        public SendAlexaNotification(IMessageBus<AutomatedMessagesBus> automatedMessageBus, ILogger<SendAlexaNotification> logger)
        {
            Condition.Requires(automatedMessageBus, nameof(automatedMessageBus)).IsNotNull();
            Condition.Requires(logger, nameof(logger)).IsNotNull();
            this._automatedMessageBus = automatedMessageBus;
            this.Logger = (ILogger<IActivity>)logger;
        }

        public new ActivityResult Invoke(IContactProcessingContext context)
        {
            Condition.Requires<IContactProcessingContext>(context, nameof(context)).IsNotNull<IContactProcessingContext>();
            try
            {
                //Send email
                SendEmail(context);

                //alexa notification
                var contact = context.Contact;
                var contactIdentifier = contact.GetAlias();

                var personalInformation = contact?.Personal();
                var contactEmail = contact?.Emails()?.PreferredEmail?.SmtpAddress;

                if (string.IsNullOrEmpty(contactEmail))
                {
                    this.Logger.LogError($"SendAlexaNotification: Error sending notification to alexa. Contact {contact?.Id} does not have any email.");
                    return new SuccessMove();
                }

                var alexaNotificationService = new AlexaNotificationService();
                alexaNotificationService.SendAbandonedCartNotificationEx(personalInformation?.Nickname);
                this.Logger.LogInformation((EventId)0, $"SendAlexaNotification: Alexa Notification sent to {contact.Id}.");

                return new SuccessMove();
            }
            catch (Exception ex)
            {
                this.Logger.LogError((EventId)0, ex, "SendAlexaNotification: Error sending notification to alexa.");
                return new SuccessStay(TimeSpan.FromMinutes(5.0));
            }
        }

        private bool SendEmail(IContactProcessingContext context)
        {
            try
            {
                AutomatedMessage automatedMessage = new AutomatedMessage()
                {
                    MessageId = this.MessageId,
                    ContactIdentifier = context.Contact.GetAlias(),
                    CustomTokens = null
                };
                if (context.ActivityEnrollment.CustomValues != null && context.ActivityEnrollment.CustomValues.Any())
                {
                    Dictionary<string, string> dictionary = new Dictionary<string, string>();
                    foreach (KeyValuePair<string, string> customValue in (IEnumerable<KeyValuePair<string, string>>)context.ActivityEnrollment.CustomValues)
                    {
                        if (!customValue.Key.StartsWith("*"))
                            dictionary.Add(customValue.Key, customValue.Value);
                    }
                    automatedMessage.CustomQueryStringParameters = (IDictionary<string, string>)dictionary;
                }
                this._automatedMessageBus.Send((object)automatedMessage, (Dictionary<string, string>)null);
                return true;
            }
            catch (Exception ex)
            {
                this.Logger.LogError((EventId)0, ex, "Error sending Commerce email.");
                return false;
            }
        }

    }
}