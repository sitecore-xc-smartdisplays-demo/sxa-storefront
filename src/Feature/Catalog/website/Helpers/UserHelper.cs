﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using IdentityModel.Client;

namespace Sitecore.HabitatHome.Feature.Catalog.Helpers
{
    public static class UserHelper
    {
        public static string LoginUser(string token)
        {
            return null;
        }

        public static string LoginUsingIdentityServer(string username, string password)
        {
            var client = new HttpClient();
            PasswordTokenRequest tokenRequest = new PasswordTokenRequest()
            {
                Address = "https://identityserver.xc-smartdisplays-demo.com/connect/token",
                ClientId = "alexa-api",
                //ClientSecret = "your_secret",
                UserName = username,
                Password = password,
                Scope = "openid EngineAPI alexa_api sitecore.profile sitecore.profile.api",
                GrantType = "password"
            };

            var response = client.RequestPasswordTokenAsync(tokenRequest).Result;
            if (!response.IsError)
            {
                return response.AccessToken;
            }
            else
            {
                throw new Exception("Invalid username or password");
            }
        }
    }
}