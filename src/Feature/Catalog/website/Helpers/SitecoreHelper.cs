﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Mvc.Common;
using Sitecore.Mvc.Pipelines;
using Sitecore.Mvc.Pipelines.Response.GetPageRendering;
using Sitecore.Mvc.Pipelines.Response.RenderRendering;
using Sitecore.Mvc.Presentation;

namespace Sitecore.HabitatHome.Feature.Catalog.Helpers
{
    public static class SitecoreHelper
    {
        public static string RenderItem(ID itemId)
        {

            var item = Sitecore.Context.Database.GetItem(itemId);

            var pageContext = new PageContext
            {
                RequestContext = HttpContext.Current.Request.RequestContext,
                Item = item
            };

            ContextService.Get().Push(pageContext);

            var pageDefinition = pageContext.PageDefinition;

            var getPageRenderingArgs = new GetPageRenderingArgs(pageDefinition);

            PipelineService.Get().RunPipeline("mvc.getPageRendering", getPageRenderingArgs);

            var rendering = getPageRenderingArgs.Result;

            var textWriter = new StringWriter();

            var renderRenderingArgs = new RenderRenderingArgs(rendering, textWriter);

            PipelineService.Get().RunPipeline("mvc.renderRendering", renderRenderingArgs);

            ContextService.Get().Pop<PageContext>();

            return textWriter.ToString();
        }
    }
}