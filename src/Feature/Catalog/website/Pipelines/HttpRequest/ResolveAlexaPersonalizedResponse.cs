﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.HabitatHome.Feature.Catalog.Factories;
using Sitecore.Mvc.Helpers;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Security.Authentication;
using Sitecore.StringExtensions;
using Sitecore.Web;
using HttpContext = System.Web.HttpContext;

namespace Sitecore.HabitatHome.Feature.Catalog.Pipelines.HttpRequest
{
    public class ResolveAlexaPersonalizedResponse : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {
            try
            {
                if (!HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("/alexa/"))
                {
                    return;
                }

                var token = HttpContext.Current.Request.Headers["Authorization"];

                if (string.IsNullOrWhiteSpace(token)) return;

                var virtualUser = VirtualUserFactory.Create(token);
                AuthenticationManager.Login(virtualUser.Name);
            }
            catch (Exception)
            {
            }
        }
    }
}