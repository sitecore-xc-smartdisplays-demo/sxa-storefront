﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Sitecore.HabitatHome.Feature.Catalog.Factories;
using Sitecore.HabitatHome.Feature.Catalog.Helpers;
using Sitecore.HabitatHome.Feature.Catalog.Personalization;
using AuthenticationManager = Sitecore.Security.Authentication.AuthenticationManager;

namespace Sitecore.HabitatHome.Feature.Catalog.ActionFilters
{

    public class AlexaUserAuthorization : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            var token = HttpContext.Current.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(token))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized));
            }

            try
            {
                var virtualUser = VirtualUserFactory.Create(token);

                AuthenticationManager.Login(virtualUser.Name);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error($"AlexaUserAuthorization Error authorizing user: {ex.Message}", this);
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized));
            }
        }
    }
}