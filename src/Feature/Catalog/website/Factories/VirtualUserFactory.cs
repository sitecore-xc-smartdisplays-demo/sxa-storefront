﻿using IdentityModel.Client;
using Newtonsoft.Json;
using Sitecore.Security.Authentication;
using System.Collections;
using System.Net.Http;

namespace Sitecore.HabitatHome.Feature.Catalog.Factories
{
    public class VirtualUserFactory
    {
        public static Sitecore.Security.Accounts.User Create(string identityServerAccessToken)
        {
            var client = new HttpClient();
            var userInfoRequest = new UserInfoRequest()
            {
                Address = "https://identityserver.xc-smartdisplays-demo.com/connect/userinfo",
                Token = identityServerAccessToken
            };

            var response = client.GetUserInfoAsync(userInfoRequest).Result;
            if (response.IsError)
                throw new Sitecore.Exceptions.AccessDeniedException();

            dynamic responseObject = JsonConvert.DeserializeObject(response.Raw);

            Sitecore.Security.Accounts.User virtualUser = AuthenticationManager.BuildVirtualUser(responseObject.name.ToString(), true);

            if (responseObject.given_name != null && responseObject.family_name != null)
                virtualUser.Profile.FullName = responseObject.given_name.ToString() + " " + responseObject.family_name.ToString();

            if (responseObject.email != null)
                virtualUser.Profile.Email = responseObject.email.ToString();

            if (responseObject.role.Type == Newtonsoft.Json.Linq.JTokenType.String)
                virtualUser.Roles.Add(Sitecore.Security.Accounts.Role.FromName(responseObject.role.ToString()));
            else
            {
                foreach (var role in responseObject.role)
                {
                    virtualUser.Roles.Add(Sitecore.Security.Accounts.Role.FromName(role.ToString()));
                }
            }

            if (responseObject.sub != null)
                virtualUser.Profile.ProfileItemId = responseObject.sub.ToString();

            return virtualUser;
        }

        public static Sitecore.Security.Accounts.User GetUser(string identityServerAccessToken)
        {
            var client = new HttpClient();
            var userInfoRequest = new UserInfoRequest()
            {
                Address = "https://identityserver.xc-smartdisplays-demo.com/connect/userinfo",
                Token = identityServerAccessToken
            };

            var response = client.GetUserInfoAsync(userInfoRequest).Result;
            if (response.IsError)
                throw new Sitecore.Exceptions.AccessDeniedException();

            dynamic responseObject = JsonConvert.DeserializeObject(response.Raw);

            var userId = responseObject.sub.ToString(); 

            return null;
        }
    }
}