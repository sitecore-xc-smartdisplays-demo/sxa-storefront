﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Sitecore.HabitatHome.Feature.Catalog.Services.Models
{
    public class AmazonNotificationData
    {
        public string timestamp { get; set; }
        public string referenceId { get; set; }
        public string expiryTime { get; set; }

        [JsonProperty("event")]
        public Event @event { get; set; }

        public RelevantAudience relevantAudience { get; set; }
    }

    public class State
    {
        public string status { get; set; }
        public string freshness { get; set; }
    }

    public class Creator
    {
        public string name { get; set; }
    }

    public class MessageGroup
    {
        public Creator creator { get; set; }
        public int count { get; set; }
    }

    public class Payload
    {
        public State state { get; set; }
        public MessageGroup messageGroup { get; set; }
    }

    public class Event
    {
        public string name { get; set; }
        public Payload payload { get; set; }
    }
    
    public class Payload2
    {
        public string user { get; set; }
    }

    public class RelevantAudience
    {
        public string type { get; set; }
        public Payload2 payload { get; set; }
    }
}