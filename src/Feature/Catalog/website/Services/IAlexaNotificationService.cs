﻿

namespace Sitecore.HabitatHome.Feature.Catalog.Services
{
    public interface IAlexaNotificationService
    {
        bool SendAbandonedCartNotification(string amazonUserId);
    }
}
