﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using Sitecore.HabitatHome.Feature.Catalog.Services.Models;

namespace Sitecore.HabitatHome.Feature.Catalog.Services
{
    public class AlexaNotificationService
    {
        /// <summary>
        /// Sends an abandoned cart notification for the alexa user id provided
        /// </summary>
        /// <param name="amazonUserId"></param>
        /// <returns></returns>
        public bool SendAbandonedCartNotification(string amazonUserId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Construct post body
                    var notificationData = new AmazonNotificationData()
                    {
                        timestamp = DateTime.Now.ToUniversalTime().ToString("u"),
                        referenceId = Guid.NewGuid().ToString(),
                        expiryTime = DateTime.Now.AddHours(1).ToUniversalTime().ToString("u"),
                        @event = new Event()
                        {
                            name = "AMAZON.MessageAlert.Activated",
                            payload = new Payload()
                            {
                                state = new State() { status = "UNREAD", freshness = "NEW" },
                                messageGroup = new MessageGroup()
                                {
                                    creator = new Creator()
                                    {
                                        name = "Andy"
                                    },
                                    count = 5
                                }
                            }
                        },
                        relevantAudience = new RelevantAudience()
                        {
                            type = "Unicast",
                            payload = new Payload2()
                            {
                                user = amazonUserId
                            }
                        }
                    };

                    //Add auth header
                    var token = GetToken();
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);

                    //Make call
                    var content = new StringContent(JsonConvert.SerializeObject(notificationData), Encoding.UTF8, "application/json");
                    var result = client.PostAsync("https://api.eu.amazonalexa.com/v1/proactiveEvents/stages/development", content).Result;

                    //TODO: check result status and return accordingly
                }

                return true;
            }
            catch (Exception ex)
            {
                //log error
                return false;
            }
        }

        public bool SendAbandonedCartNotificationEx(string amazonUserId)
        {
            try
            {
                SendAlexaNotificationMessage();

                var token = GetTokenEx();

                //TODO: Refactor 
                var client = new RestClient("https://api.amazonalexa.com/v1/proactiveEvents/stages/development");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Content-Length", "802");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", "api.amazonalexa.com");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token.access_token}");
                request.AddParameter("undefined", "{\n    \"timestamp\": \"" + (DateTime.Now.ToString("s") + "Z")  + "\",\n    \"referenceId\": \"" + Guid.NewGuid().ToString() + "\",\n    \"expiryTime\": \"" + ((DateTime.Now.AddHours(1).ToString("s") + "Z")) + "\",\n    \"event\": {\n    \"name\": \"AMAZON.MessageAlert.Activated\",\n    \"payload\": {\n      \"state\": {\n        \"status\": \"UNREAD\",\n        \"freshness\": \"NEW\"\n      },\n      \"messageGroup\": {\n        \"creator\": {\n          \"name\": \"Habitat Grocers\"\n        },\n        \"count\": 1\n      }\n    }\n\t},\n      \"relevantAudience\": {\n        \"type\": \"Unicast\",\n        \"payload\": {\n        \t\"user\": \"" + amazonUserId + "\"\n        }\n    }\n}", ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);

                return response.IsSuccessful;
            }
            catch (Exception ex)
            {
                //log error
                return false;
            }
        }

        private bool SendAlexaNotificationMessage()
        {
            try
            {
                //Sending request to aws lambda
                //TODO: Refactor 
                var client = new RestClient("https://xxxxxxxx.execute-api.us-east-1.amazonaws.com/Beta/addMessage");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Content-Length", "152");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", "xxxxxxxx.execute-api.us-east-1.amazonaws.com");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Content-Type", "text/plain");
                request.AddParameter("undefined", "{\n\t\"messageId\": \"" + Guid.NewGuid() + "\",\n\t\"messageBody\": \"We noticed you have items in your cart. Would you like to continue shopping?\"\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                return response.IsSuccessful;
            }
            catch (Exception ex)
            {
                //log error
                return false;
            }
        }

        private AmazonToken  GetToken()
        {
            //TODO: Refactor 
            var clientId = "<your_client_id>";
            var clientSecret = "<your_client_secret>";

            using (var client = new HttpClient())
            {
                var values = new Dictionary<string, string>
                {
                    { "grant_type", "client_credentials" },
                    { "client_id", clientId },
                    { "client_secret", clientSecret },
                    { "scope", "alexa::proactive_events" }
                };

                var content = new FormUrlEncodedContent(values);

                var response = client.PostAsync("https://api.amazon.com/auth/O2/token", content).Result;

                var responseString = response.Content.ReadAsStringAsync().Result;

                return JsonConvert.DeserializeObject<AmazonToken>(responseString);
            }
        }

        private AmazonToken GetTokenEx()
        {
            //TODO: Refactor 
            var clientId = "<your_client_id>";
            var clientSecret = "<your_client_secret>";

            var client = new RestClient("https://api.amazon.com/auth/O2/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("undefined", $"grant_type=client_credentials&client_id={clientId}&client_secret={clientSecret}&scope=alexa%3A%3Aproactive_events", ParameterType.RequestBody);
            var response = client.Execute<AmazonToken>(request);

            return response.Data;
        }

    }
}