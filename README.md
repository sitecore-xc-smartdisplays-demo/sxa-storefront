# BACKGROUND
This project is a customization/extension of the HabitatHome Commerce Demo based on Sitecore XP/XC 9.1.1. More details  on HabitatHome Commerce can be found here: https://github.com/Sitecore/Sitecore.HabitatHome.Commerce.
Note that you need to install Habibtat Home Platform first from here: https://github.com/Sitecore/Sitecore.Demo.Platform/tree/release/9.1.1

HabitatHome Commerce has been extended to showcase how it can be integrated with Amazon Echo Show for voice commerce. This code was used for the demo for 'The future of voice commerce with smart displays powered by Sitecore Experience Commerce' talk at Sitecore Symposium '19:
https://www.sitecore.com/knowledge-center/blog/2019/11/future-of-voice-commerce
https://sitecoredude.com/the-future-of-voice-commerce-with-smart-displays-powered-by-sitecore-experience-commerce/

The three use cases that are implemented here are:
1) Product search and check out. A complete handsfree transaction powered by Sitecore Experience Commerce.
2) Personalized upselling. All voice content managed and personalized via Sitecore’s powerful CMS capabilities.
3) Cross-channel abandoned cart recapture. By extending Sitecore’s Marketing Automation capabilities, we created the ability to push notifications from Sitecore Experience Commerce to the Alexa Show Device.

Note that this is experimental work in progress code developed specifically for the POC, and contains some hardcoding that needs to be removed (tagged with TODOs). 
This should be used in conjunction with the Amzaon Echo Show code repo which you can find in the same group as this project: https://gitlab.com/sitecore-xc-smartdisplays-demo/amazon-echo-show. 

# DISCLAIMER
All source code, data and information provided in this repository is for informational purposes only. I make no representations as to the accuracy, completeness, currentness, suitability, 
or validity of any information herein and will not be liable for any errors, omissions, or delays in this information or any losses, injuries, or damages arising from its display or use. 
All information is provided on an as-is basis.

# KEY AREAS OF CUSTOMIZATION
Note that all new functionality to support the Alexa/Amazon Echo Show integration including catalog, cart, checkout etc have all been placed in the following project for simplicity. These should be split out into the respective feature projects.
Project: Sitecore.HabitatHome.Feature.Catalog.Website 

src\Feature\Catalog\website\ActionFilters\AlexaUserAuthorization.cs
src\Feature\Catalog\website\App_Config\Include\Feature\Commerce\Feature.Catalog.config
src\Feature\Catalog\website\Controllers\
src\Feature\Catalog\website\Factories\
src\Feature\Catalog\website\Helpers\
src\Feature\Catalog\website\MarketingAutomation\readme.txt
src\Feature\Catalog\website\MarketingAutomation\
src\Feature\Catalog\website\Models\AlexaResponsePhraseModel.cs
src\Feature\Catalog\website\Personalization
src\Feature\Catalog\website\Pipelines\
src\Feature\Catalog\website\Repositories\AlexaResponsePhraseRepository.cs
src\Feature\Catalog\website\Repositories\IAlexaResponsePhraseRepository.cs
src\Feature\Catalog\website\ServiceCollection\RegisterCatalogServices.cs
src\Feature\Catalog\website\Services\



# HABITATHOME COMMERCE INSTALLATION INSTRUCTIONS

# Introduction  
HabitatHome Commerce Demo and the tools and processes in it is a Sitecore&reg; solution example built using Sitecore Experience Accelerator&trade; (SXA) on Sitecore Experience Platform&trade; (XP) and Sitecore Experience Commerce&trade; (XC) following the Helix architecture principles.

# Important Notice

### License
Please read the LICENSE carefully prior to using the code in this repository
 
### Support

The code, samples and/or solutions provided in this repository are ***unsupported by Sitecore PSS***. Support is provided on a best-effort basis via GitHub issues or [Slack #habitathome](https://sitecorechat.slack.com/messages/habitathome/) (see end of README for additional information).

It is assumed that you already have a working instance of Sitecore XP **and** Sitecore XC  and all prerequisites prior to installing the demo. Support for **product installation** issues should be directed to relevant Community channels or through regular Sitecore support channels. 

### Warranty

The code, samples and/or solutions provided in this repository are for example purposes only and **without warranty (expressed or implied)**. The code has not been extensively tested and is not guaranteed to be bug free.  

# Getting Started

**This guide assumes you've cloned and deployed Sitecore.HabitatHome.Platform. See the README.md file in the [Sitecore.HabitatHome.Platform](https://github.com/sitecore/sitecore.habitathome.platform) repository.**

## Prerequisites

### Sitecore Version

Prior to attempting the demo installation, ensure you have a working **Sitecore XC 9.0.2** instance. Detailed installation instructions can be found at [doc.sitecore.com](http://commercesdn.sitecore.net/SitecoreXC_9.0/Installation-Guide/9.0.2/SitecoreXC-9.0_Installation_Guide(On-Prem).pdf).

You do not need to install the Storefront Theme

*IMPORTANT: Publish site after installation*

**Clone this repository**

## Custom Install - before you start

If you do **not want to use the default settings**, you need to adjust the appropriate values in the `/cake-config.json` file.


Note: If you've already deployed the HabitatHome Platform demo, and you wish to run the HabitatHome Commerce demo in a new instance by customizing these sttings,
you would need to also customize the settings in the HabitatHome Platform demo and deploy it to the new instance **before** deploying HabitatHome Commerce. 
See README.md in Sitecore.HabitatHome.Platform for custom settings.

## Installation
**All installation instructions assume using PowerShell 5.1 in administrative mode.**

### 1 Clone the Repository
Clone the Sitecore.HabitatHome.Commerce repository locally - default settings assume **`C:\Projects\Sitecore.HabitatHome.Commerce`**. 

`git clone https://github.com/Sitecore/Sitecore.HabitatHome.Commerce.git` or 
`git clone git@github.com:Sitecore/Sitecore.HabitatHome.Commerce.git`
  
### 2 Deploy Solution
Note: If you have not yet done so, deploy the base HabitatHome.Platform solution. See README.md in Sitecore.HabitatHome.Platform
You can run the `Quick-Deploy` gulp task from the root of the HabitatHome.Platform solution for a quicker deploy that excludes post deploy actions or Unicorn synchronization.

To deploy the **HabitatHome.Commerce** solution, from the root of the solution

`.\build.ps1 -Target Initial`

_Notes:_

- Build target **Initial** only needs to be executed successfully during the initial deployment. Subsequent deployments can be made by running the **Default** Cake build target: `.\build.ps1` (without target specification).

### 3 Deploy Engine

The next step will deploy Habitat Home's custom Commerce Engine with its relevant plugin and load the catalog, inventory and promotions.

_Notes:_
* If you want to use your own engine suffix rather than `habitathome`, you need to update it in `deploy-commerce-engine.ps1`
* If you want to use your own databases rather than `habitathome_Global`, you need to update it in `\src\Project\HabitatHome\engine\wwwroot\bootstrap\Global.json`
* If you want to use your own databases rather than `habitathome_SharedEnvironments`, you need to update it in `\src\Project\HabitatHome\engine\wwwroot\data\Environments\Plugin.SQL.PolicySet-1.0.0.json`
The script is provided as an example and should be reviewed to understand its behavior prior to its execution. In summary, the script:

- Compiles and publishes the engine to a temporary location (default .\publishTemp)
- Makes changes to the configuration files to correctly set the certificate thumbprint, hostnames, etc)
- Stops IIS
- Creates a backup of the engine folders
- Copies the published and modified engine files to the webroot
- Starts IIS 
- Bootstraps the environment
- Cleans the environment (** erases all Commerce-related *data*)
- Initializes Environment (imports demo catalog, promotions, inventory, etc)


Assuming you have the default installation settings:

`.\deploy-commerce-engine.ps1 -Bootstrap -Initialize`

> if you have made any changes to your settings, review the `deploy-engine-commerce.ps1` script and override / modify the parameters as required.


# Contribute or Issues
Please post any issues on Slack Community [#habitathome](https://sitecorechat.slack.com/messages/habitathome/) channel or create an issue on [GitHub](https://github.com/Sitecore/Sitecore.HabitatHome.Commerce/issues). Contributions are always welcome!
